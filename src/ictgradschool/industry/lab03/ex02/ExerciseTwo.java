package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {
    //private String lowerBound;
    //private String upperBound;
    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        System.out.println("lower bound? ");
        int lowerBound = Integer.parseInt(Keyboard.readInput());

        System.out.println("upper bound? ");
        int upperBound = Integer.parseInt(Keyboard.readInput());
        System.out.println("The LowerBound is "+ lowerBound + " and the UpperBound is " + upperBound);

        int range1 = Math.round(upperBound - lowerBound);
        int random1 = (int) Math.round(lowerBound + (range1 * Math.random()));

        int range2 = Math.round(upperBound - lowerBound);
        int random2 = (int) Math.round(lowerBound + (range2 * Math.random()));

        int range3 = Math.round(upperBound - lowerBound);
        int random3 = (int) Math.round(lowerBound + (range2 * Math.random()));

        Math.min(Math.min(random1, random2), random3);


    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
