package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 * <p>
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 * <li>Printing the prompt and reading the amount from the user</li>
 * <li>Printing the prompt and reading the number of decimal places from the user</li>
 * <li>Truncating the amount to the user-specified number of DP's</li>
 * <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {
        String number = getAmount();
        int dps = getDecimalPlaces();
        String truncated = truncate(number, dps);
        printTruncated(truncated, dps);
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    public String getAmount() {
        System.out.println("Enter an amount ");
        return Keyboard.readInput();
    }


    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    public int getDecimalPlaces() {
        System.out.println("Enter a number of decimal places ");
        int numDps = Integer.parseInt(Keyboard.readInput());
        return numDps;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    public String truncate(String number, int dps) {
        int dpIndex = number.indexOf('.');
        String truncated = number.substring(0, dpIndex + dps + 1);
        return truncated;
    }


    // TODO Write a method which prints the truncated amount
    public void printTruncated(String truncated, int dps) {
        System.out.println("Amount truncated to " + dps + " decimal places is: " + truncated);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
